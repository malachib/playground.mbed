#include "mbed-drivers/mbed.h"

//#if SAM
//#define MAL_LED PA17
//#else

// since I don't have compiler defines for YOTTA figured out, churning/hardcoding
// here and commenting out #if
// FOR SAMW25:
#define MAL_LED LED1
//#endif

static void blinky(void) {
    static DigitalOut led(MAL_LED);
    led = !led;
    printf("LED = %d \r\n",led.read());
}

void app_start(int, char**) {
    minar::Scheduler::postCallback(blinky).period(minar::milliseconds(2000));
}
