#include "Adafruit_SSD1306.h"

// ST303K8 pinouts
#define MOSI PA_7
#define MISO PA_6
#define CLK PA_5

#define CS PA_4
#define RST A1
#define DC A0


SPI spi(MOSI, MISO, CLK);
Adafruit_SSD1306_Spi display(spi, DC, RST, CS, 64);


void oled_debug_counter()
{
    static int counter = 0;
    display.clearDisplay();
#if defined(GFX_WANT_ABSTRACTS) or defined(GFX_SIZEABLE_TEXT)
    display.setTextSize(2);
#endif
    display.setTextCursor(0, 0);
    display.printf("Count %d", counter++);
    display.display();
}

void oled_setup()
{
    //spi.frequency(1000000);
    spi.frequency(250000);
    spi.format(8,3);

    display.printf("G");
    display.display();
}
