#include <mbed.h>
#include <mbed_events.h>

#if SAM
#define MAL_LED PA17
#else
#define MAL_LED LED1
#endif

#define DC_RFM PA_8


DigitalOut dc_rfm69(DC_RFM);

static void blinky(void) {
    static DigitalOut led(MAL_LED);
    led = !led;
    printf("LED = %d \r\n",led.read());
}

void oled_debug_counter();
void oled_setup();

int main()
{
    EventQueue queue;

    dc_rfm69 = 1; // active-low, so set off

    oled_setup();

    queue.call_in(2000, printf, "called in 2 seconds!");
    queue.call_every(1000, blinky);
    queue.call_every(1000, oled_debug_counter);

    queue.dispatch();
}
