#include "mbed.h"
// Have to explicitly include our own copy here, and its header #ifdef
// keeps subsequent TextLCD from including its own
//#include "TextLCD_config.h"
#include "TextLCD.h"

//I2C i2c_lcd(PB_9, PB_8);
I2C i2c_lcd(I2C_SDA, I2C_SCL);
//TextLCD_I2C lcd(&i2c_lcd, 0x3f, TextLCD::LCD16x2);
//TextLCD_I2C lcd(&i2c_lcd, 0x27, TextLCD::LCD16x2);
Serial pc(USBTX, USBRX);

void i2c_scan()
{
    pc.printf("RUN\r\n");
    I2C& i2c = i2c_lcd;
    for(int i = 0; i < 128 ; i++) {
        i2c.start();
        if(i2c.write(i << 1)) pc.printf("0x%x ACK \r\n",i); // Send command string
        i2c.stop();
    }
}

int i2c_scan2()
{
    bool foundIt = false;
    pc.printf("RUN\r\n");
    I2C& i2c = i2c_lcd;
    for(int i = 0; i < 128 ; i++) {
        i2c.start();
        if(i2c.write(i << 1)) foundIt = true;
        i2c.stop();
        if(foundIt) return i;
    }
}

int main()
{
    static int counter = 0;

    int lcd_addr = i2c_scan2();

    pc.printf("Found at 0x%x\r\n", lcd_addr);

    TextLCD_I2C lcd(&i2c_lcd, lcd_addr << 1, TextLCD::LCD16x2);

    lcd.setBacklight(TextLCD::LightOn);

    for(;;)
    {
        lcd.locate(0, 0);
        lcd.printf("Alive: %d", counter);
        pc.printf("Alive: %d\r\n", counter++);
        wait(1);
    }
    return 0;
}
