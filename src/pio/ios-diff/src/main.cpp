#include "mbed.h"

/*

Official IOSTREAM yields 193k flash, shim flavor is 51k
even 51k seems high however.  PRINTF_ONLY version is 39k

 */


#ifndef PRINTF_ONLY
#ifndef FEATURE_IOSTREAM
#include <fact/iostream.h>

using namespace FactUtilEmbedded::std;

Serial pc(USBTX, USBRX);

namespace FactUtilEmbedded { namespace std {

ostream cout(pc);
ostream& clog = cout;

}}
#else
#include <iostream>

using namespace std;
#endif

inline ostream& eol(ostream& __os)
{ return __os.write("\r\n", 2); }
#endif

int main()
{
    static int counter = 0;

    for(;;)
    {
#ifdef PRINTF_ONLY
        printf("Waiting...%d\r\n", counter++);
#else
        clog << "Waiting..." << counter++ << eol;
#endif
        wait(1.0);
    }
    return 0;
}
