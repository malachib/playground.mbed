#include <mbed.h>
#include <mbed_events.h>


static void blinky(void) {
    static DigitalOut led(LED1);
    led = !led;
    printf("Blink: %d\r\n", (int)led);
}

void oled_debug_counter();
void oled_setup();

void rfm69_setup();
void rfm69_loop();
void rfm69_debug();

int main()
{
    EventQueue queue;

    oled_setup();
    rfm69_setup();

    queue.call_every(1000, blinky);
    queue.call_every(500, oled_debug_counter);
    queue.call_every(5000, rfm69_debug);

    // NOTE: this made it ornery... 5ms every-call shouldn't bring it
    // down, but it does
    //queue.call_every(5, rfm69_loop);

    for(;;)
    {
        queue.dispatch(3000);
        rfm69_loop();
    }
}
