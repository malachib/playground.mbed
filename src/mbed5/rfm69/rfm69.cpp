#include <RFM69.h>


// NUCLEO 303K8 pins
#define MOSI PA_7
#define MISO PA_6
#define CLK PA_5
// NSS = CS line
#define NSS D9
#define INTERRUPT D8
#define RST NC

RFM69 radio(MOSI, MISO, CLK, NSS, INTERRUPT);

#define NODEID 123
#define NETWORKID     100  //the same on all nodes that talk to each other
#define ENCRYPTKEY "sampleEncryptKey" // must be exactly 16 characters


// LOW POWER LAB port
void rfm69_setup()
{
    printf("Initializing radio\r\n");

    if(!radio.initialize(RF69_915MHZ,NODEID, NETWORKID))
        printf("Initialization failed\r\n");

    printf("Radio type = %x\r\n", radio.readReg(0x10)); // *should* be 0x24

    radio.sleep();
    radio.setHighPower();
    radio.setPowerLevel(14);
    radio.encrypt(ENCRYPTKEY);

    // For RADIOHEAD test mode, but I doubt that will work easily
    char key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                      0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
    //radio.encrypt(key);
    //radio.promiscuous();

    printf("RSSI = %d\r\n", radio.readRSSI());

    // also doesn't work, kind of a longshot expecting sending to ourself
    // to work anyway
    //radio.send(NODEID, "Hi", 2);

    /*
     retry flavor won't work yet because we don't make it to loop to
     service ACK
    if(radio.sendWithRetry(NODEID, "Hi", 2))
    {
        printf("Successfully sent\n");
    } */
}



void rfm69_debug()
{
    uint8_t radio_temp;

    printf("rfm69 temperature: ");

    radio_temp = radio.readTemperature(); // get CMOS temperature (8bit)

    printf("%d\r\n", radio_temp);
}

// LOW POWER LAB port
void rfm69_loop()
{
    printf("Checking radio\r\n");

    wait(0.1);

    if(radio.receiveDone())
    {
        printf("Got something\r\n");

        if(radio.ACKRequested())
        {
            radio.sendACK();
            printf("Sent ack\r\n");
        }
    }
}
