#!/bin/bash

export MBED_TARGET=$(mbed target | sed 's/\[mbed\] //')
export MBED_TOOLCHAIN=$(mbed toolchain | sed 's/\[mbed\] //')
#echo "Debug: $MBED_TARGET / $MBED_TOOLCHAIN"
PROJECT_NAME=software-serial
#result=$(st-flash write BUILD/NUCLEO_F401RE/GCC_ARM/$PROJECT_NAME.bin 0x8000000)
st-flash write BUILD/$MBED_TARGET/$MBED_TOOLCHAIN/$PROJECT_NAME.bin 0x8000000
#echo $result
