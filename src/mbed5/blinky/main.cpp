#include "mbed.h"
#include "mbed_events.h"

#if SAM
#define MAL_LED PA17
#else
#define MAL_LED LED1
#endif

DigitalOut led1(MAL_LED);

// main() runs in its own thread in the OS
// (note the calls to Thread::wait below for delays)
int main() {
    while (true) {
        led1 = !led1;
        wait(0.5);
        //Thread::wait(500);
    }
}
