// We're doing events now as per
// https://github.com/armmbed/mbed-events
// https://forums.mbed.com/t/how-to-use-scheduler-minar-with-mbed-os-5/1836/2
#include <mbed.h>
#include <mbed_events.h>

#if SAM
#define MAL_LED PA17
#else
#define MAL_LED LED1
#endif

static void blinky(void) {
    static DigitalOut led(MAL_LED);
    led = !led;
    printf("LED = %d \r\n",led.read());
}

/* old mbed OS 3.0 one here
void app_start(int, char**) {
    minar::Scheduler::postCallback(blinky).period(minar::milliseconds(500));
}
*/

int main()
{
  EventQueue queue;

  queue.call_in(2000, printf, "called in 2 seconds!");
  queue.call_every(1000, blinky);
}
