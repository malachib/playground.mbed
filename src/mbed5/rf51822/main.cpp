#include "mbed.h"
#include "mbed_events.h"

#if SAM
#define MAL_LED PA17
#else
#define MAL_LED LED1
#endif

DigitalOut led1(MAL_LED);
Serial pc(USBTX, USBRX);

// main() runs in its own thread in the OS
// (note the calls to Thread::wait below for delays)
int main() {
    pc.baud(9600);
    while (true) {
        led1 = !led1;
        wait(1.0);
        pc.printf("Led = %d", (int)led1);
        //Thread::wait(500);
    }
}
