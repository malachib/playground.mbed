#include <mbed.h>

#ifdef TARGET_SAMD21
static SPI spi(MBED_SPI0);
#else
static SPI spi(SPI_MOSI, SPI_MISO, SPI_SCK, SPI_CS);
#endif
